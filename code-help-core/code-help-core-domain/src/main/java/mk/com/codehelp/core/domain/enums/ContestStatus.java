package mk.com.codehelp.core.domain.enums;

public enum ContestStatus {
  OPEN, STARTED, CLOSED
}
