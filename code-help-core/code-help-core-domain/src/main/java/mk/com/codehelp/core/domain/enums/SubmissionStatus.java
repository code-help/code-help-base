package mk.com.codehelp.core.domain.enums;

public enum SubmissionStatus {
  ACCEPTED,
  DECLINED
}
