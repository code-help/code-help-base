package mk.com.codehelp.core.domain.enums;

public enum Difficulty {
  EASY,
  MEDIUM,
  HARD
}
