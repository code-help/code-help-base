package com.codehelp.forum.domain

enum class UserRole(val role: String) {

  ADMIN("role_admin");
}