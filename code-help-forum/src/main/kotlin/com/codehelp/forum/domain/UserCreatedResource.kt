package com.codehelp.forum.domain

interface UserCreatedResource {

  val user: User
}